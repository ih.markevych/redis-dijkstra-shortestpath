def get_edges_and_weights(r, db_name, source_node_id):
    res = r.execute_command('GRAPH.QUERY', db_name, "MATCH (r)-[e]->(c) WHERE r.id = " + str(source_node_id) + " RETURN c.id, e.w")[1]
    return [(r[0], float(r[1])) for r in res]

def get_edges_and_weights_reversed(r, db_name, destination_node_id):
    res = r.execute_command('GRAPH.QUERY', db_name, "MATCH (c)-[e]->(r) WHERE r.id = " + str(destination_node_id) + " RETURN c.id, e.w")[1]
    return [(r[0], float(r[1])) for r in res]

def get_all_ids(r, db_name):
    import numpy as np
    return np.ravel(r.execute_command('GRAPH.QUERY', db_name, "MATCH (r) RETURN r.id")[1])

def find_path_with_precomputed(db_name, source_node_id, destination_node_id, costs):
    '''
    Returns path from source node to destination node with smallest total cost.
    There are two assumptions for a database:
    1. Each node should contain `id` property with unique id.
    2. Each relation should contain `w`property with non-negative numeric value that represents cost of transitioning from source node to destination node.
    
    Arguments:
    db_name: name of database on which search will be done. 
    source_node_id: id of node from which path should be started.
    destination node id: id of node on which path should end.
    costs: precomputed with `find_all_costs` costs of reaching each node in form of a dictionary with pairs {id: cost} for each node.
    
    Returns:
    path - list of found path with minimal cost (starts from source_node_id and ends with destination_node_id).    
    '''
    import redis
    
    r = redis.StrictRedis()
    
    path = [destination_node_id]
    while path[0] != source_node_id:
        prev_cost = costs[path[0]]
        neighbors = get_edges_and_weights_reversed(r, db_name, path[0])
        for n_id, n_w in neighbors:
            if prev_cost - n_w == costs[n_id]:
                path.insert(0, n_id)
                break
    return path

def find_path(db_name, source_node_id, destination_node_id):
    '''
    Returns path and cost to get from source node to destination node.
    There are two assumptions for a database:
    1. Each node should contain `id` property with unique id.
    2. Each relation should contain `w`property with non-negative numeric value that represents cost of transitioning from source node to destination node.
    
    Arguments:
    db_name: name of database on which search will be done. 
    source_node_id: id of node from which path should be started.
    destination node id: id of node on which path should end.
    
    Returns:
    Tuple (path, cost), where
    path is a list of found path with minimal cost (starts from source_node_id and ends with destination_node_id).
    cost is float that is a cost of a path.
    '''
    
    import redis
    import numpy as np

    r = redis.StrictRedis()
    
    costs = {}
    unvisited = set(get_all_ids(r, db_name))

    if source_node_id not in unvisited:
        raise Exception("Source node id can't be found!")
    
    if destination_node_id not in unvisited:
        raise Exception("Destination node id can't be found!")
    
    for i in unvisited:
        costs[i] = float('inf')

    costs[source_node_id] = 0

    while len(unvisited) > 0:

        source = min(unvisited, key=lambda key: costs[key])
        source_cost = costs[source]

        if source == destination_node_id:
            break

        if source_cost == float('inf'):
            raise Exception("Destination node can't be reached from source node!")
            break

        neighbors = get_edges_and_weights(r, db_name, source)

        for n_id, n_w in neighbors:
            new_cost = source_cost + n_w
            if new_cost < costs[n_id]:
                costs[n_id] = new_cost

        unvisited.remove(source)

    final_cost = costs[destination_node_id]

    path = find_path_with_precomputed(db_name, source_node_id, destination_node_id, costs)
    
    return path, final_cost

def find_all_costs(db_name, source_node_id):
    '''
    Returns costs to get from source node to any node.
    There are two assumptions for a database:
    1. Each node should contain `id` property with unique id.
    2. Each relation should contain `w`property with non-negative numeric value that represents cost of transitioning from source node to destination node.
    
    Arguments:
    db_name: name of database on which search will be done. 
    source_node_id: id of node from which path should be started.
    
    Returns:
    Dictionary with node_id: cost to node with node_id from source node. 
    '''
    
    import redis
    import numpy as np

    r = redis.StrictRedis()
    
    costs = {}
    unvisited = set(get_all_ids(r, db_name))
    
    if source_node_id not in unvisited:
        raise Exception("Source node id can't be found!")
        
    for i in unvisited:
        costs[i] = float('inf')

    costs[source_node_id] = 0

    while len(unvisited) > 0:

        source = min(unvisited, key=lambda key: costs[key])
        source_cost = costs[source]

        neighbors = get_edges_and_weights(r, db_name, source)

        for n_id, n_w in neighbors:
            new_cost = source_cost + n_w
            if new_cost < costs[n_id]:
                costs[n_id] = new_cost

        unvisited.remove(source)

    return costs

print(find_path('City', 1, 3))
print(find_all_costs('City', 1))
